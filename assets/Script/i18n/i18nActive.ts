import { i18nMgr } from "./i18nMgr";
const { ccclass, menu } = cc._decorator;

@ccclass
@menu("多语言/i18nActive")
export class i18nActive extends cc.Component {

    /**记录当前需要显示的子节点位置 */
    lastActiveNum: number = 0;

    start() {
        i18nMgr._addOrDelActive(this, true);
        for (let i = 0; i < this.node.children.length; i++) {
            this.node.children[i].active = false;
        }
        this._resetValue();
    }

    _resetValue() {
        i18nMgr._getActive((language) => {
            this.node.children[this.lastActiveNum].active = false;
            for (let i = 0; i < this.node.children.length; i++) {
                if (this.node.children[i].name == language) {
                    this.lastActiveNum = i;
                    this.node.children[i].active = true;
                    break;
                }
            }
        });
    }

    onDestroy() {
        i18nMgr._addOrDelActive(this, false);
    }
}
