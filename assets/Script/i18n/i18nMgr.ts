import * as i18nLabel from "./i18nLabel";
import * as i18nActive from "./i18nActive";
import * as i18nLabelRichTex from "./i18nLabelRichTex";
import * as i18nSprite from "./i18nSprite";

export class i18nMgr {
    private static language = "";     // 当前语言
    private static labelArr: i18nLabel.i18nLabel[] = [];        // i18nLabel 列表
    private static labelRichTexlArr: i18nLabelRichTex.i18nLabelRichTex[] = [];        // i18nLabelRichTex 列表
    private static labelData: { [key: string]: string } = {};   // 普通文字配置
    private static labelRichTexlData: { [key: string]: string } = {};   // 富文本文字配置
    private static spriteArr: i18nSprite.i18nSprite[] = [];       // i18nSprite 列表
    private static activeArr: i18nActive.i18nActive[] = [];       // i18nActive 列表

    private static checkInit() {
        if (!this.language) {
            this.setLanguage("zh");
        }
    }

    /**
     * 设置语言
     */
    public static setLanguage(language: string) {
        if (this.language === language) {
            return;
        }
        this.language = language;
        this.reloadLabel();
        this.reloadLabelRichTex();
        this.reloadSprite();
        this.reloadActive();
    }

    /**
     * 添加或移除 i18nLabel
     */
    public static _addOrDelLabel(label: i18nLabel.i18nLabel, isAdd: boolean) {
        if (isAdd) {
            this.labelArr.push(label);
        } else {
            let index = this.labelArr.indexOf(label);
            if (index !== -1) {
                this.labelArr.splice(index, 1);
            }
        }
    }

    /**
     * 添加或移除 i18nActive
     */
    public static _addOrDelActive(label: i18nActive.i18nActive, isAdd: boolean) {
        if (isAdd) {
            this.activeArr.push(label);
        } else {
            let index = this.activeArr.indexOf(label);
            if (index !== -1) {
                this.labelArr.splice(index, 1);
            }
        }
    }

    /**
    * 添加或移除 i18nLabelRichTex
    */
    public static _addOrDelLabelRichTex(label: i18nLabelRichTex.i18nLabelRichTex, isAdd: boolean) {
        if (isAdd) {
            this.labelRichTexlArr.push(label);
        } else {
            let index = this.labelRichTexlArr.indexOf(label);
            if (index !== -1) {
                this.labelRichTexlArr.splice(index, 1);
            }
        }
    }

    public static _getLabelRichTex(opt: string, params: string[]): string {
        this.checkInit();
        if (params.length === 0) {
            return this.labelRichTexlData[opt] || opt;
        }
        let str = this.labelRichTexlData[opt] || opt;
        for (let i = 0; i < params.length; i++) {
            let reg = new RegExp("#" + i, "g")
            str = str.replace(reg, params[i]);
        }
        return str;
    }

    public static _getLabel(opt: string, params: string[]): string {
        this.checkInit();
        if (params.length === 0) {
            return this.labelData[opt] || opt;
        }
        let str = this.labelData[opt] || opt;
        for (let i = 0; i < params.length; i++) {
            let reg = new RegExp("#" + i, "g")
            str = str.replace(reg, params[i]);
        }
        return str;
    }


    /**
     * 添加或移除 i18nSprite
     */
    public static _addOrDelSprite(sprite: i18nSprite.i18nSprite, isAdd: boolean) {
        if (isAdd) {
            this.spriteArr.push(sprite);
        } else {
            let index = this.spriteArr.indexOf(sprite);
            if (index !== -1) {
                this.spriteArr.splice(index, 1);
            }
        }
    }

    public static _getSprite(path: string, cb: (spriteFrame: cc.SpriteFrame) => void) {
        this.checkInit();
        cc.resources.load("i18n/sprite/" + this.language + "/" + path, cc.SpriteFrame, (err, spriteFrame: cc.SpriteFrame) => {
            if (err) {
                return cb(null);
            }
            cb(spriteFrame);
        });
    }

    public static _getActive(cb: (language: string) => void) {
        this.checkInit();
        cb(this.language);
    }


    private static reloadLabel() {
        let url = "i18n/label/" + this.language;
        cc.resources.load(url, (err, data: cc.JsonAsset) => {
            if (err) {
                console.error(err);
                this.labelData = {};
            } else {
                this.labelData = data.json;
            }
            for (let one of this.labelArr) {
                one._resetValue();
            }
        });
    }

    private static reloadLabelRichTex() {
        let url = "i18n/labelRichTex/" + this.language;
        cc.resources.load(url, (err, data: cc.JsonAsset) => {
            if (err) {
                console.error(err);
                this.labelRichTexlData = {};
            } else {
                this.labelRichTexlData = data.json;
            }
            for (let one of this.labelRichTexlArr) {
                one._resetValue();
            }
        });
    }

    private static reloadSprite() {
        for (let one of this.spriteArr) {
            one._resetValue();
        }
    }

    private static reloadActive() {
        for (let one of this.activeArr) {
            one._resetValue();
        }
    }

}
